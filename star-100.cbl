       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Star-100.
       AUTHOR. Son.

       PROCEDURE DIVISION.
       000-Begin.
           PERFORM 10 TIMES 
              PERFORM 001-Print-Star-Inline THRU  001-Exit 
           END-PERFORM
           GOBACK 
           . 

       001-Print-Star-Inline.
           PERFORM 002-Print-One-Star 10 TIMES 
           DISPLAY " "
           .

       001-Exit.
           EXIT 
           .

       002-Print-One-Star.
           DISPLAY "*" WITH NO ADVANCING 
           .