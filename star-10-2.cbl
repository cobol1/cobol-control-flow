       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Star-10-2.
       AUTHOR. Son.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  Count-Num    PIC   9(2)  VALUE ZEROS.

       PROCEDURE DIVISION.
       000-Begin.
           PERFORM 001-Print-Star-Inline THRU  001-Exit. 
      *    PERFORM 002-Print-Star-Outline  THRU  002-Exit. 
           GOBACK 
           . 

       001-Print-Star-Inline.
           PERFORM 002-Print-One-Star WITH TEST BEFORE UNTIL 
              Count-Num = 10
           DISPLAY " "
           .

       001-Exit.
           EXIT 
           .

       002-Print-One-Star.
           DISPLAY "*" WITH NO ADVANCING 
           COMPUTE Count-Num = Count-Num + 1
           .

      *002-Exit.
      *    EXIT 
      *    .

      *003-Print-One-Star.
      *    DISPLAY "*" WITH NO ADVANCING 
      *    .
