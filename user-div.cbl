       IDENTIFICATION DIVISION. 
       PROGRAM-ID. User-Div.
       AUTHOR. Son.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  Num1     PIC 9(5)       VALUE 0.
       01  Num2     PIC 9(5)       VALUE 0.
           88 Num2IsZero VALUE 0.
       01  Result   PIC 9(5)V9(3)  VALUE 0.

       PROCEDURE DIVISION.
       000-Begin.
           PERFORM 001-User-Div THRU 001-Exit 
           GOBACK. 
       001-User-Div.
           DISPLAY "Please input Num1 :" WITH NO ADVANCING 
           ACCEPT Num1 
           DISPLAY "Please input Num2 :" WITH NO ADVANCING 
           ACCEPT Num2
           IF Num2IsZero THEN
              DISPLAY "Error: Num2 is zero."
              GO TO 001-Exit 
           END-IF 
           COMPUTE Result = Num1 / Num2
           DISPLAY "Result is " Result 
           .
       001-User-Display-End.
           DISPLAY "End of User-Div".
       001-Exit.
           EXIT .
