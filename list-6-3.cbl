       IDENTIFICATION DIVISION. 
       PROGRAM-ID. List-6-3.
       AUTHOR. Son.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  Rep-Count         PIC 9(4).
       01  Prn-Rep-Count     PIC Z,ZZ9.
       01  Number-of-Times   PIC 9(4) VALUE 1000.

       PROCEDURE DIVISION .
       Begin.
           PERFORM VARYING Rep-Count FROM 0 BY 50
              UNTIL Rep-Count = Number-of-Times 
              MOVE Rep-Count TO Prn-Rep-Count 
              DISPLAY "counting" Prn-Rep-Count 
           END-PERFORM
           MOVE Rep-Count TO Prn-Rep-Count 
           DISPLAY "If I have told you once."
           DISPLAY "I've told you " Prn-Rep-Count " times."
           GOBACK
       .
