       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Triangle-1.
       AUTHOR. Son.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  Scr-Line       PIC X(80) VALUE SPACES .
       01  Star-Num       PIC 9(3) VALUE ZEROS .
           88 Vaild-Star-Num VALUE 1 THRU 80.
       01  Index-Num      PIC 9(3) VALUE ZEROS .
       01  Index-Num2     PIC 9(3) VALUE ZEROS .

       PROCEDURE DIVISION .
       000-Begin.
           PERFORM 001-Input-Star-Num THRU 001-Exit
           PERFORM VARYING Index-Num FROM 1 BY 1
              UNTIL Index-Num > Star-Num
              COMPUTE  Index-Num2 = Star-Num - Index-Num + 1
              MOVE ALL "*" TO Scr-Line(Index-Num2:Index-Num )
              DISPLAY Scr-Line 
           END-PERFORM
           GOBACK 
           .
       001-Input-Star-Num.
           PERFORM UNTIL Vaild-Star-Num 
              DISPLAY "Please input star number: "    WITH NO ADVANCING 
              ACCEPT Star-Num 
              IF NOT Vaild-Star-Num 
              DISPLAY "Please input star number in positive number"  
              END-IF
           END-PERFORM
           . 
       001-Exit.
           EXIT 
           .
