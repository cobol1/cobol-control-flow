       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Triangle-1.
       AUTHOR. Son.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  Scr-Line       PIC X(80) VALUE SPACES .
       01  Star-Num       PIC 9(3) VALUE ZEROS .
           88 Vaild-Star-Num VALUE 1 THRU 80.
       01  Index-Num      PIC 9(3) VALUE ZEROS .

       PROCEDURE DIVISION .
       000-Begin.
           Perform 002-Input-Star-Num THRU 002-Exit
           Perform 001-Print-Star-Line THRU 001-Exit
              VARYING Index-Num FROM 1 by 1 
              UNTIL Index-Num > Star-Num
           GOBACK .

       001-Print-Star-Line.
           MOVE ALL "*" TO Scr-Line(1:Index-Num )
           DISPLAY Scr-Line 
           .
       001-Exit.
           EXIT.

       002-Input-Star-Num.
           PERFORM UNTIL Vaild-Star-Num 
              DISPLAY "Please input star number: "    WITH NO ADVANCING 
              ACCEPT Star-Num 
              IF NOT Vaild-Star-Num 
              DISPLAY "Please input star number in positive number"  
              END-IF
           END-PERFORM
           . 
       002-Exit.
           EXIT 
           .
