       IDENTIFICATION DIVISION. 
       PROGRAM-ID. List-6-4.
       AUTHOR. Son.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  Counters.
           02 HundredsCount     PIC 99   VALUE ZEROS .
           02 TensCount         PIC 99   VALUE ZEROS .
           02 UnitsCount        PIC 99   VALUE ZEROS .

       01  Odometer.
           02 PrnHundreds       PIC 9.
           02 FILLER            PIC X VALUE "-".
           02 PrnTens           PIC 9.
           02 FILLER            PIC X VALUE "-".
           02 PrnUnits          PIC 9.

       PROCEDURE DIVISION .
       000-Begin.
           DISPLAY "Using an out-of-line Perform"
           PERFORM 001-CountMileage THRU 001-Exit 
               VARYING HundredsCount FROM 0 BY 1 UNTIL HundredsCount > 9
               AFTER TensCount FROM 0 BY 1 UNTIL TensCount > 9
               AFTER UnitsCount FROM 0 BY 1 UNTIL UnitsCount > 9
           GOBACK
           .
       001-CountMileage.
           MOVE HundredsCount TO PrnHundreds 
           MOVE TensCount     TO PrnTens 
           MOVE UnitsCount    TO PrnUnits 
           DISPLAY "Out - " Odometer 
           .
       001-Exit.
           EXIT .
