       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Square-Star.
       AUTHOR. Son.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  Scr-Line       PIC X(80) VALUE SPACES .
       01  Star-Num       PIC 9(3) VALUE  ZEROS .

       PROCEDURE DIVISION .
       000-Begin.
           Perform 002-Input-Star-Num THRU 002-Exit
           Perform 001-Print-Star-Line THRU 001-Exit Star-Num TIMES
           GOBACK .

       001-Print-Star-Line.
           MOVE ALL "*" TO Scr-Line(1:Star-Num )
           DISPLAY Scr-Line 
           .
       001-Exit.
           EXIT.

       002-Input-Star-Num.
           PERFORM UNTIL Star-Num > 0
              DISPLAY "Please input star number: "    WITH NO ADVANCING 
              ACCEPT Star-Num 
              IF Star-Num = 0 DISPLAY "Please input star number in posi"
              "tive number"  
              END-IF
           END-PERFORM
           . 
       002-Exit.
           EXIT 
           .
