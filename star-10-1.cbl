       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Star-10-1.
       AUTHOR. Son.

       PROCEDURE DIVISION.
       000-Begin.
           PERFORM 001-Print-Star-Inline THRU  001-Exit. 
           PERFORM 002-Print-Star-Outline  THRU  002-Exit. 
           GOBACK 
           . 

       001-Print-Star-Inline.
           PERFORM 10 TIMES 
           DISPLAY "*" WITH NO ADVANCING 
           END-PERFORM 
           DISPLAY " "
           .

       001-Exit.
           EXIT 
           .

       002-Print-Star-Outline.
           PERFORM 003-Print-One-Star 10 TIMES 
           DISPLAY " "
           .

       002-Exit.
           EXIT 
           .

       003-Print-One-Star.
           DISPLAY "*" WITH NO ADVANCING 
           .
